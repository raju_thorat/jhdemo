import { Moment } from 'moment';

export interface IEmp {
  id?: number;
  name?: string;
  city?: string;
  createdBy?: string;
  createdDate?: Moment;
  lastModifiedBy?: string;
  lastModifiedDate?: Moment;
}

export class Emp implements IEmp {
  constructor(
    public id?: number,
    public name?: string,
    public city?: string,
    public createdBy?: string,
    public createdDate?: Moment,
    public lastModifiedBy?: string,
    public lastModifiedDate?: Moment
  ) {}
}
