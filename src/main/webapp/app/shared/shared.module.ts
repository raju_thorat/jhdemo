import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { JhdemoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [JhdemoSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [JhdemoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhdemoSharedModule {
  static forRoot() {
    return {
      ngModule: JhdemoSharedModule
    };
  }
}
