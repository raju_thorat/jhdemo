import { NgModule } from '@angular/core';

import { JhdemoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [JhdemoSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [JhdemoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class JhdemoSharedCommonModule {}
