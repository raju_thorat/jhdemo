import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IEmp } from 'app/shared/model/emp.model';

type EntityResponseType = HttpResponse<IEmp>;
type EntityArrayResponseType = HttpResponse<IEmp[]>;

@Injectable({ providedIn: 'root' })
export class EmpService {
  public resourceUrl = SERVER_API_URL + 'api/emps';

  constructor(protected http: HttpClient) {}

  create(emp: IEmp): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(emp);
    return this.http
      .post<IEmp>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(emp: IEmp): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(emp);
    return this.http
      .put<IEmp>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEmp>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEmp[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(emp: IEmp): IEmp {
    const copy: IEmp = Object.assign({}, emp, {
      createdDate: emp.createdDate != null && emp.createdDate.isValid() ? emp.createdDate.toJSON() : null,
      lastModifiedDate: emp.lastModifiedDate != null && emp.lastModifiedDate.isValid() ? emp.lastModifiedDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate != null ? moment(res.body.createdDate) : null;
      res.body.lastModifiedDate = res.body.lastModifiedDate != null ? moment(res.body.lastModifiedDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((emp: IEmp) => {
        emp.createdDate = emp.createdDate != null ? moment(emp.createdDate) : null;
        emp.lastModifiedDate = emp.lastModifiedDate != null ? moment(emp.lastModifiedDate) : null;
      });
    }
    return res;
  }
}
