import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IEmp, Emp } from 'app/shared/model/emp.model';
import { EmpService } from './emp.service';

@Component({
  selector: 'jhi-emp-update',
  templateUrl: './emp-update.component.html'
})
export class EmpUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    city: [],
    createdBy: [],
    createdDate: [],
    lastModifiedBy: [],
    lastModifiedDate: []
  });

  constructor(protected empService: EmpService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ emp }) => {
      this.updateForm(emp);
    });
  }

  updateForm(emp: IEmp) {
    this.editForm.patchValue({
      id: emp.id,
      name: emp.name,
      city: emp.city,
      createdBy: emp.createdBy,
      createdDate: emp.createdDate != null ? emp.createdDate.format(DATE_TIME_FORMAT) : null,
      lastModifiedBy: emp.lastModifiedBy,
      lastModifiedDate: emp.lastModifiedDate != null ? emp.lastModifiedDate.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const emp = this.createFromForm();
    if (emp.id !== undefined) {
      this.subscribeToSaveResponse(this.empService.update(emp));
    } else {
      this.subscribeToSaveResponse(this.empService.create(emp));
    }
  }

  private createFromForm(): IEmp {
    return {
      ...new Emp(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      city: this.editForm.get(['city']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdDate:
        this.editForm.get(['createdDate']).value != null ? moment(this.editForm.get(['createdDate']).value, DATE_TIME_FORMAT) : undefined,
      lastModifiedBy: this.editForm.get(['lastModifiedBy']).value,
      lastModifiedDate:
        this.editForm.get(['lastModifiedDate']).value != null
          ? moment(this.editForm.get(['lastModifiedDate']).value, DATE_TIME_FORMAT)
          : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmp>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
