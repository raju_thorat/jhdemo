/**
 * View Models used by Spring MVC REST controllers.
 */
package com.raju.web.rest.vm;
