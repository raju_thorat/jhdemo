package com.raju.web.rest;

import com.raju.service.EmpService;
import com.raju.web.rest.errors.BadRequestAlertException;
import com.raju.service.dto.EmpDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.raju.domain.Emp}.
 */
@RestController
@RequestMapping("/api")
public class EmpResource {

    private final Logger log = LoggerFactory.getLogger(EmpResource.class);

    private static final String ENTITY_NAME = "emp";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmpService empService;

    public EmpResource(EmpService empService) {
        this.empService = empService;
    }

    /**
     * {@code POST  /emps} : Create a new emp.
     *
     * @param empDTO the empDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new empDTO, or with status {@code 400 (Bad Request)} if the emp has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/emps")
    public ResponseEntity<EmpDTO> createEmp(@RequestBody EmpDTO empDTO) throws URISyntaxException {
        log.debug("REST request to save Emp : {}", empDTO);
        if (empDTO.getId() != null) {
            throw new BadRequestAlertException("A new emp cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmpDTO result = empService.save(empDTO);
        return ResponseEntity.created(new URI("/api/emps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /emps} : Updates an existing emp.
     *
     * @param empDTO the empDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated empDTO,
     * or with status {@code 400 (Bad Request)} if the empDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the empDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/emps")
    public ResponseEntity<EmpDTO> updateEmp(@RequestBody EmpDTO empDTO) throws URISyntaxException {
        log.debug("REST request to update Emp : {}", empDTO);
        if (empDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EmpDTO result = empService.save(empDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, empDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /emps} : get all the emps.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of emps in body.
     */
    @GetMapping("/emps")
    public ResponseEntity<List<EmpDTO>> getAllEmps(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Emps");
        Page<EmpDTO> page = empService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /emps/:id} : get the "id" emp.
     *
     * @param id the id of the empDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the empDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/emps/{id}")
    public ResponseEntity<EmpDTO> getEmp(@PathVariable Long id) {
        log.debug("REST request to get Emp : {}", id);
        Optional<EmpDTO> empDTO = empService.findOne(id);
        return ResponseUtil.wrapOrNotFound(empDTO);
    }

    /**
     * {@code DELETE  /emps/:id} : delete the "id" emp.
     *
     * @param id the id of the empDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/emps/{id}")
    public ResponseEntity<Void> deleteEmp(@PathVariable Long id) {
        log.debug("REST request to delete Emp : {}", id);
        empService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
