package com.raju.service;

import com.raju.service.dto.EmpDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.raju.domain.Emp}.
 */
public interface EmpService {

    /**
     * Save a emp.
     *
     * @param empDTO the entity to save.
     * @return the persisted entity.
     */
    EmpDTO save(EmpDTO empDTO);

    /**
     * Get all the emps.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EmpDTO> findAll(Pageable pageable);


    /**
     * Get the "id" emp.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EmpDTO> findOne(Long id);

    /**
     * Delete the "id" emp.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
