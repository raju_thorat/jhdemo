package com.raju.service.impl;

import com.raju.service.EmpService;
import com.raju.domain.Emp;
import com.raju.repository.EmpRepository;
import com.raju.service.dto.EmpDTO;
import com.raju.service.mapper.EmpMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Emp}.
 */
@Service
@Transactional
public class EmpServiceImpl implements EmpService {

    private final Logger log = LoggerFactory.getLogger(EmpServiceImpl.class);

    private final EmpRepository empRepository;

    private final EmpMapper empMapper;

    public EmpServiceImpl(EmpRepository empRepository, EmpMapper empMapper) {
        this.empRepository = empRepository;
        this.empMapper = empMapper;
    }

    /**
     * Save a emp.
     *
     * @param empDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EmpDTO save(EmpDTO empDTO) {
        log.debug("Request to save Emp : {}", empDTO);
        Emp emp = empMapper.toEntity(empDTO);
        emp = empRepository.save(emp);
        return empMapper.toDto(emp);
    }

    /**
     * Get all the emps.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EmpDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Emps");
        return empRepository.findAll(pageable)
            .map(empMapper::toDto);
    }


    /**
     * Get one emp by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EmpDTO> findOne(Long id) {
        log.debug("Request to get Emp : {}", id);
        return empRepository.findById(id)
            .map(empMapper::toDto);
    }

    /**
     * Delete the emp by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Emp : {}", id);
        empRepository.deleteById(id);
    }
}
