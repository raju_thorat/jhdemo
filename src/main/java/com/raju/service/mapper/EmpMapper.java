package com.raju.service.mapper;

import com.raju.domain.*;
import com.raju.service.dto.EmpDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Emp} and its DTO {@link EmpDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EmpMapper extends EntityMapper<EmpDTO, Emp> {



    default Emp fromId(Long id) {
        if (id == null) {
            return null;
        }
        Emp emp = new Emp();
        emp.setId(id);
        return emp;
    }
}
