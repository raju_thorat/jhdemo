package com.raju.web.rest;

import com.raju.JhdemoApp;
import com.raju.domain.Emp;
import com.raju.repository.EmpRepository;
import com.raju.service.EmpService;
import com.raju.service.dto.EmpDTO;
import com.raju.service.mapper.EmpMapper;
import com.raju.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.raju.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link EmpResource} REST controller.
 */
@SpringBootTest(classes = JhdemoApp.class)
public class EmpResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private EmpRepository empRepository;

    @Autowired
    private EmpMapper empMapper;

    @Autowired
    private EmpService empService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEmpMockMvc;

    private Emp emp;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EmpResource empResource = new EmpResource(empService);
        this.restEmpMockMvc = MockMvcBuilders.standaloneSetup(empResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Emp createEntity(EntityManager em) {
        Emp emp = new Emp()
            .name(DEFAULT_NAME)
            .city(DEFAULT_CITY)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
        return emp;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Emp createUpdatedEntity(EntityManager em) {
        Emp emp = new Emp()
            .name(UPDATED_NAME)
            .city(UPDATED_CITY)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        return emp;
    }

    @BeforeEach
    public void initTest() {
        emp = createEntity(em);
    }

    @Test
    @Transactional
    public void createEmp() throws Exception {
        int databaseSizeBeforeCreate = empRepository.findAll().size();

        // Create the Emp
        EmpDTO empDTO = empMapper.toDto(emp);
        restEmpMockMvc.perform(post("/api/emps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(empDTO)))
            .andExpect(status().isCreated());

        // Validate the Emp in the database
        List<Emp> empList = empRepository.findAll();
        assertThat(empList).hasSize(databaseSizeBeforeCreate + 1);
        Emp testEmp = empList.get(empList.size() - 1);
        assertThat(testEmp.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEmp.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testEmp.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testEmp.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testEmp.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testEmp.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createEmpWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = empRepository.findAll().size();

        // Create the Emp with an existing ID
        emp.setId(1L);
        EmpDTO empDTO = empMapper.toDto(emp);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmpMockMvc.perform(post("/api/emps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(empDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Emp in the database
        List<Emp> empList = empRepository.findAll();
        assertThat(empList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEmps() throws Exception {
        // Initialize the database
        empRepository.saveAndFlush(emp);

        // Get all the empList
        restEmpMockMvc.perform(get("/api/emps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(emp.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getEmp() throws Exception {
        // Initialize the database
        empRepository.saveAndFlush(emp);

        // Get the emp
        restEmpMockMvc.perform(get("/api/emps/{id}", emp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(emp.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEmp() throws Exception {
        // Get the emp
        restEmpMockMvc.perform(get("/api/emps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEmp() throws Exception {
        // Initialize the database
        empRepository.saveAndFlush(emp);

        int databaseSizeBeforeUpdate = empRepository.findAll().size();

        // Update the emp
        Emp updatedEmp = empRepository.findById(emp.getId()).get();
        // Disconnect from session so that the updates on updatedEmp are not directly saved in db
        em.detach(updatedEmp);
        updatedEmp
            .name(UPDATED_NAME)
            .city(UPDATED_CITY)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        EmpDTO empDTO = empMapper.toDto(updatedEmp);

        restEmpMockMvc.perform(put("/api/emps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(empDTO)))
            .andExpect(status().isOk());

        // Validate the Emp in the database
        List<Emp> empList = empRepository.findAll();
        assertThat(empList).hasSize(databaseSizeBeforeUpdate);
        Emp testEmp = empList.get(empList.size() - 1);
        assertThat(testEmp.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEmp.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testEmp.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testEmp.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testEmp.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testEmp.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingEmp() throws Exception {
        int databaseSizeBeforeUpdate = empRepository.findAll().size();

        // Create the Emp
        EmpDTO empDTO = empMapper.toDto(emp);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmpMockMvc.perform(put("/api/emps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(empDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Emp in the database
        List<Emp> empList = empRepository.findAll();
        assertThat(empList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEmp() throws Exception {
        // Initialize the database
        empRepository.saveAndFlush(emp);

        int databaseSizeBeforeDelete = empRepository.findAll().size();

        // Delete the emp
        restEmpMockMvc.perform(delete("/api/emps/{id}", emp.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Emp> empList = empRepository.findAll();
        assertThat(empList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Emp.class);
        Emp emp1 = new Emp();
        emp1.setId(1L);
        Emp emp2 = new Emp();
        emp2.setId(emp1.getId());
        assertThat(emp1).isEqualTo(emp2);
        emp2.setId(2L);
        assertThat(emp1).isNotEqualTo(emp2);
        emp1.setId(null);
        assertThat(emp1).isNotEqualTo(emp2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmpDTO.class);
        EmpDTO empDTO1 = new EmpDTO();
        empDTO1.setId(1L);
        EmpDTO empDTO2 = new EmpDTO();
        assertThat(empDTO1).isNotEqualTo(empDTO2);
        empDTO2.setId(empDTO1.getId());
        assertThat(empDTO1).isEqualTo(empDTO2);
        empDTO2.setId(2L);
        assertThat(empDTO1).isNotEqualTo(empDTO2);
        empDTO1.setId(null);
        assertThat(empDTO1).isNotEqualTo(empDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(empMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(empMapper.fromId(null)).isNull();
    }
}
